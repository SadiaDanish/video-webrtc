'use strict';

var isChannelReady = false;
var isInitiator = false;
var isStarted = false;
var localStream;
var pc;
var remoteStream;
var turnReady;
var socket;
var room ='';
var ipAddress = 'https://192.168.1.114:8888/';
var localVideo = document.querySelector('#local_video');
var remoteVideo = document.querySelector('#remote_video');

var constraints = {
  video: true,
  audio:true
};

var pcConfig = {
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  }]
};

// Set up audio and video regardless of what devices are present.
var sdpConstraints = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

//Room selection view on page loading....
document.addEventListener('DOMContentLoaded', roomSelectionView(), false);

function joinRoom_click(evt){
  var validInput = false;
  room = document.getElementById('txtRoomNo').value;
  validInput = validateInput(room);
  if(validInput)
  {
    //Display share room link div
    displaySharingInfo (room, document.URL + '?id=' + room);
    connect(room);
  }
}

function joinRandomRoom_click() {
  var randomNumber = Math.floor(Math.random() * 90000) + 10000;
  txtRoomNo.value = randomNumber;
}

function confirmJoin_click(){
  var confirmJoinDiv = document.getElementById('confirm-join-div');
  confirmJoinDiv.classList.add("hidden");
  var urlString = document.URL;
  var url = new URL(urlString);
  var room = url.searchParams.get("id");

//  room = document.getElementById('txtRoomNo').value;
  connect(room);
}
function connect(room){


    navigator.mediaDevices.getUserMedia({
      audio: false,
      video: true
    })
    .then(gotStream)
    .catch(function(e) {
      log_error('getUserMedia() error: ' + e.name);
    });
    log('Getting user media with constraints', constraints);



  socket = io.connect();
  console.log("---Open Socket -----");

    socket.emit('create or join', room);
    log('Attempted to create or  join room', room);

  socket.on('created', function(room) {
  log('Created room ' + room);
  isInitiator = true;
  });
    socket.on('full', function(room) {
    log('Room ' + room + ' is full');
  });

    socket.on('join', function (room){
    log('Another peer made a request to join room ' + room);
    log('This peer is the initiator of room ' + room + '!');
    isChannelReady = true;
  });

    socket.on('joined', function(room) {
    log('joined: ' + room);
    isChannelReady = true;
  });

    socket.on('log', function(array) {
    console.log.apply(console, array);
  });
    // This client receives a message
    socket.on('message', function(message) {
    log('Client received message:', message);
    if (message === 'got user media') {
      maybeStart();
    } else if (message.type === 'offer') {
      if (!isInitiator && !isStarted) {
        maybeStart();
      }
      pc.setRemoteDescription(new RTCSessionDescription(message));
      doAnswer();
    } else if (message.type === 'answer' && isStarted) {
      pc.setRemoteDescription(new RTCSessionDescription(message));
    } else if (message.type === 'candidate' && isStarted) {
      var candidate = new RTCIceCandidate({
        sdpMLineIndex: message.label,
        candidate: message.candidate
      });
      pc.addIceCandidate(candidate);
    } else if (message === 'bye' && isStarted) {
      handleRemoteHangup();
    }
  });

  if (location.hostname !== 'localhost') {
    requestTurn(
      'https://computeengineondemand.appspot.com/turn?username=41784574&key=4080218913'
    );
  }
}

function sendMessage(message) {
  log('Client sending message: ', message);
  socket.emit('message', message);
}

function gotStream(stream) {
  log('Adding local stream.');
  localStream = stream;
  localVideo.srcObject = stream;
  sendMessage('got user media');
  if (isInitiator) {
    maybeStart();
  }
}


function maybeStart() {
  log('>>>>>>> maybeStart() ', isStarted, localStream, isChannelReady);
  if (!isStarted && typeof localStream !== 'undefined' && isChannelReady) {
    log('>>>>>> creating peer connection');
    createPeerConnection();
    pc.addStream(localStream);
    isStarted = true;
    log('isInitiator', isInitiator);
    if (isInitiator) {
      doCall();
    }
  }
}

window.onbeforeunload = function() {
  sendMessage('bye');
};

/////////////////////////////////////////////////////////

function createPeerConnection() {
  try {
    pc = new RTCPeerConnection(null);
    pc.onicecandidate = handleIceCandidate;
    pc.onaddstream = handleRemoteStreamAdded;
    pc.onremovestream = handleRemoteStreamRemoved;
    log('Created RTCPeerConnnection');
  } catch (e) {
    log_error('Failed to create PeerConnection, exception: ' + e.message);
    alert('Cannot create RTCPeerConnection object.');
    return;
  }
}

function handleIceCandidate(event) {
  log('icecandidate event: ', event);
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    });
  } else {
    log('End of candidates.');
  }
}

function handleCreateOfferError(event) {
  log('createOffer() error: ', event);
}

function doCall() {
  log('Sending offer to peer');
  pc.createOffer(setLocalAndSendMessage, handleCreateOfferError);
}

function doAnswer() {
  log('Sending answer to peer.');
  pc.createAnswer().then(
    setLocalAndSendMessage,
    onCreateSessionDescriptionError
  );
}

function setLocalAndSendMessage(sessionDescription) {
  pc.setLocalDescription(sessionDescription);
  log('setLocalAndSendMessage sending message', sessionDescription);
  sendMessage(sessionDescription);
}

function onCreateSessionDescriptionError(error) {
  log_error('Failed to create session description: ' + error.toString());
}

function requestTurn(turnURL) {
  var turnExists = false;
  for (var i in pcConfig.iceServers) {
    if (pcConfig.iceServers[i].urls.substr(0, 5) === 'turn:') {
      turnExists = true;
      turnReady = true;
      break;
    }
  }
  if (!turnExists) {
    log('Getting TURN server from ', turnURL);
    // No TURN server. Get one from computeengineondemand.appspot.com:
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var turnServer = JSON.parse(xhr.responseText);
        console.log('Got TURN server: ', turnServer);
        pcConfig.iceServers.push({
          'urls': 'turn:' + turnServer.username + '@' + turnServer.turn,
          'credential': turnServer.password
        });
        turnReady = true;
      }
    };
    xhr.open('GET', turnURL, true);
    xhr.send();
  }
}

function handleRemoteStreamAdded(event) {
  log('Remote stream added.');
  remoteStream = event.stream;
  remoteVideo.srcObject = remoteStream;
}

function handleRemoteStreamRemoved(event) {
  log('Remote stream removed. Event: ', event);
}

function hangup() {
  log('Hanging up.');
  stop();
  sendMessage('bye');
}

function handleRemoteHangup() {
  log('Session terminated.');
  stop();
  isInitiator = false;
}

function stop() {
  isStarted = false;
  pc.close();
  pc = null;
}
function hangUp_click(){
  hangup();
}
