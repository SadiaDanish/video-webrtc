//On page load, decide which view has to be shown to user, if url contain room
//id, it will be forwarded to join conversation otherwise it will be taken
//to room selection page...

function roomSelectionView(){
  var urlString = document.URL;
  var url = new URL(urlString);
  var roomSelectionDiv = document.getElementById('room-selection');
  var confirmJoinDiv = document.getElementById('confirm-join-div');
  var roomParam = url.searchParams.get("id");
  if (!roomParam){
    roomSelectionDiv.classList.remove("hidden");
  } else {
    confirmJoinDiv.classList.remove("hidden");
    }
}

//Validate user input for room number
function validateInput(roomNo){

  var btnJoin = document.getElementById('btnJoin');
  var btnRandom = document.getElementById('btnRandom')
  var txtRoomNo = document.getElementById('txtRoomNo');
  var lblRoomNoError = document.getElementById('lblRoomNoError');

  // Validate room id, enable/disable join button.
  // The server currently accepts only the \w character class and
  // hyphen+underscor.
  var valid = roomNo.length >= 5;
  var re = /^([a-zA-Z0-9-_]+)+$/;
  valid = valid && re.exec(roomNo);
  if (valid) {
    btnJoin.disabled = true;
    btnRandom.disabled = true;
    txtRoomNo.classList.remove('invalid');
    lblRoomNoError.classList.add('hidden');
  } else {
  btnJoin.disabled = false;
  txtRoomNo.value = null;
  txtRoomNo.classList.add('invalid');
  lblRoomNoError.classList.remove('hidden');
  }
  return valid;
}
//Show link to peer user on the current page
function displaySharingInfo(roomId, roomLink) {
  var shareLinkDiv = document.getElementById('sharing-div')
  var roomLinkHref = document.getElementById('linkRoom');

  roomLinkHref.href = roomLink;
  roomLinkHref.text = roomId;
  activateElement(shareLinkDiv);
};

function hideElement (element) {
  element.classList.add('hidden');
};

function showElement  (element) {
  element.classList.remove('hidden');
};

function activateElement (element) {
  element.classList.add('active');
};

function deactivateElement (element) {
  element.classList.remove('active');
};

function setRoomNumber() {
  return document.getElementById("txtRoomNo").value;
}
